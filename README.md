# Prova prática

Esta é a prova prática de Devops para o componente de Tópicos Especiais em Tecnologia do curso Técnico em Informática da IENH.

Esta prova consiste em desempenhar de forma prática a criação de um pipeline (Bitbucket) de testes unitários automatizados com uso de .NET Core (C#) e Nunit.

## Passos da prova

1. Efetuar um fork deste projeto de forma pública;
2. Alterar este projeto para aceitar multiplos projetos com uso de solution;
3. Criar um projeto de testes unitários (Nunit);
4. Criar os testes unitarios para a classe **Validador**;
5. Criar um pipeline (Bitbucket) para executar os testes unitários de forma automática.

## Condições da prova

- Prova **individual**;
- Permitido a consulta aos materiais de aula e a internet;
- O aluno que terminar a prova só poderá sair no inicio do intervalo e esta dispensado;
- Dúvidas chamem o professor.

## Nota

O aluno será considerado **Apto** se concluir as 5 etapas de forma funcional, isto é, com um fork público deste projeto, com os testes sendo efetuados com sucesso e rodando de forma automatizada via pipeline (demonstar com sucesso e com erro).
